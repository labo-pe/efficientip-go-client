package ipam

import (
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

var _ Ipam = &SolidServer{}
var _ IpamBuilder = &SolidServerBuilder{}

func TestDnsRRList(t *testing.T) {
	server := createServer("../tests/ipam/dns_rr_list.json")
	defer server.Close()

	config := getConfig(server.URL)
	result, _ := config.DnsRRList("*.test.intra")

	assert.Equal(t, "*.test.intra", result[0]["rr_full_name"])
}

func TestIpAliasList(t *testing.T) {
	server := createServer("../tests/ipam/ip_alias_list.json")
	defer server.Close()

	config := getConfig(server.URL)
	result, _ := config.IpAliasList("1")

	assert.Equal(t, "test.labo-pe.intra", result[0]["name"])
	assert.Equal(t, "*.alias.k8s.labo-pe.intra", result[0]["alias_name"])
}

func TestIpAliasAdd(t *testing.T) {
	server := createServer("../tests/ipam/ip_alias_add.json")
	defer server.Close()

	config := getConfig(server.URL)
	result, _ := config.IpAliasAdd("ipTest", "10.10.10.10", "SITE TEST")

	assert.Equal(t, "1", result[0]["ret_oid"])
}

func TestIpAliasDelete(t *testing.T) {
	server := createServer("../tests/ipam/ip_alias_delete.json")
	defer server.Close()

	config := getConfig(server.URL)
	result, _ := config.IpAliasDelete("ipAliasTest")

	assert.Equal(t, "1", result[0]["ret_oid"])
}

func TestGetAddressIPAndName(t *testing.T) {
	server := createServer("../tests/ipam/get_address_free.json")
	defer server.Close()

	config := getConfig(server.URL)
	result, err := config.getAddress("test", "10.10.10.10")

	assert.Equal(t, "", result)
	assert.Equal(t, nil, err)
}

func TestGetAddressIPAndNameAlreadyExists(t *testing.T) {
	server := createServer("../tests/ipam/get_address.json")
	defer server.Close()

	config := getConfig(server.URL)
	result, err := config.getAddress("test", "10.10.10.10")

	assert.Equal(t, "10.10.10.10", result)
	assert.Equal(t, nil, err)
}

func TestGetAddressIPAlreadyExistWithOtherName(t *testing.T) {
	server := createServer("../tests/ipam/get_address.json")
	defer server.Close()

	config := getConfig(server.URL)
	result, err := config.getAddress("test2", "10.10.10.10")

	assert.Equal(t, "", result)
	assert.Equal(t, IpAlreadyExists(IpAlreadyExists{Ip: "10.10.10.10", Name: "test"}), err)
}

func TestGetAddressIPOnlyNotExisting(t *testing.T) {
	server := createServer("../tests/ipam/get_address_free.json")
	defer server.Close()

	config := getConfig(server.URL)
	result, err := config.getAddress("", "10.10.10.10")

	assert.Equal(t, "", result)
	assert.Equal(t, nil, err)
}

func TestGetAddressIPOnlyExisting(t *testing.T) {
	server := createServer("../tests/ipam/get_address.json")
	defer server.Close()

	config := getConfig(server.URL)
	result, err := config.getAddress("", "10.10.10.10")

	assert.Equal(t, "", result)
	assert.Equal(t, IpAlreadyExists(IpAlreadyExists{Ip: "10.10.10.10", Name: "test"}), err)
}

func TestIpPoolList(t *testing.T) {
	server := createServer("../tests/ipam/pools.json")
	defer server.Close()

	config := getConfig(server.URL)
	result, _ := config.IpPoolList("", 1)

	assert.Equal(t, "pool1", result[0]["pool_name"])
}

func TestIpFindFreeOneIPNoBeginEnd(t *testing.T) {
	server := createServer("../tests/ipam/ip_find_free_1.json")
	defer server.Close()

	config := getConfig(server.URL)
	result, _ := config.IpFindFree("1")

	assert.Equal(t, "10.10.10.152", result[0]["hostaddr"])
}

func TestIpAddNoName(t *testing.T) {
	server := createServer("../tests/ipam/ip_add.json")
	defer server.Close()

	config := getConfig(server.URL)
	result, _ := config.IpAdd("SITE TEST", "10.10.10.10", "", "", nil)

	assert.Equal(t, "1", result[0]["ret_oid"])
}

func TestIpAddWithClassAndParameters(t *testing.T) {
	server := createServer("../tests/ipam/ip_add.json")
	defer server.Close()

	config := getConfig(server.URL)
	result, _ := config.IpAdd("SITE TEST", "10.10.10.10", "", "CLASS", &map[string]string{"param1": "value1"})

	assert.Equal(t, "1", result[0]["ret_oid"])
}

func TestGetIp(t *testing.T) {
	server := createServer("../tests/ipam/ip_address_list.json")
	defer server.Close()

	config := getConfig(server.URL)
	result, _ := config.GetIp("811221")

	assert.Equal(t, "test-tdd.labo-pe.intra", result["name"])
	assert.Equal(t, "811221", result["ip_id"])
}

func TestGetIpQuandIpNExistePas(t *testing.T) {
	server := createServer("../tests/ipam/ip_address_list_inexistant.json")
	defer server.Close()

	config := getConfig(server.URL)
	result, _ := config.GetIp("811221")

	assert.Nil(t, result)
}

func TestGetIpIpIdPasseVide(t *testing.T) {
	config := getConfig("")
	result, _ := config.GetIp("")
	assert.Nil(t, result)
}

func TestIpAddressListWithNameOnly(t *testing.T) {
	server := createServer("../tests/ipam/ip_address_list.json")
	defer server.Close()

	config := getConfig(server.URL)
	result, _ := config.IpAddressList("test-tdd.labo-pe.intra", "", 1)

	assert.Equal(t, "test-tdd.labo-pe.intra", result[0]["name"])
}

func TestIpAddressListWithIpOnly(t *testing.T) {
	server := createServer("../tests/ipam/ip_address_list.json")
	defer server.Close()

	config := getConfig(server.URL)
	result, _ := config.IpAddressList("", "10.10.10.10", 1)

	assert.Equal(t, "test-tdd.labo-pe.intra", result[0]["name"])
}

func TestIpDelete(t *testing.T) {
	server := createServer("../tests/ipam/ip_delete.json")
	defer server.Close()

	config := getConfig(server.URL)
	result, _ := config.IpDelete("id")

	assert.Equal(t, "1", result[0]["ret_oid"])
}

func getConfig(url string) SolidServer {
	config := SolidServer{
		Url:               url,
		SkipSslValidation: true}

	return config
}

func createServer(file string) *httptest.Server {
	bytes, _ := os.ReadFile(file)
	// Start a local HTTP server
	return httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		rw.Write(bytes)
	}))
}
