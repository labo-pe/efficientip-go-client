package ipam

import (
	"fmt"
)

type IpAlreadyExists struct {
	Ip   string
	Name string
}

type SolidServerError struct {
	StatusCode int
	Method     string
	Endpoint   string
	Parameters map[string]string
	Message    []byte
}

type NoFreeIpFound struct {
	PoolId string
}

type IpNotFound struct {
	Ip         string
	Occurences int
}

type PoolNotFound struct {
	Pool       string
	Occurences int
}

func (e IpAlreadyExists) Error() string {
	return fmt.Sprintf("IP %s, already reserved with name %s", e.Ip, e.Name)
}

func (e SolidServerError) Error() string {
	return fmt.Sprintf("SolidServer responded with status %d\nMethod: %s, Endpoint: %s\nParameters: %s\nMessage: %v",
		e.StatusCode,
		e.Method,
		e.Endpoint,
		e.Parameters,
		string(e.Message))
}

func (e NoFreeIpFound) Error() string {
	return fmt.Sprintf("Pool %s has returned an empty list of free IPs", e.PoolId)
}

func (e IpNotFound) Error() string {
	return fmt.Sprintf("Found %d instances for IP %s", e.Occurences, e.Ip)
}

func (e PoolNotFound) Error() string {
	return fmt.Sprintf("Found %d instances for Pool %s", e.Occurences, e.Pool)
}
