package ipam

type Ipam interface {
	Checker
	IpsManager
	AliasManager
}

type IpsManager interface {
	ReserveIP(name string, ip string, poolName string, siteName string, class string, classParameters *map[string]string) (map[string]string, error)
	DeleteIp(ip string) error
	GetIp(ipId string) (map[string]string, error)
}

type AliasManager interface {
	IpAliasDelete(ipNameId string) ([]map[string]string, error)
	AddAlias(ip string, fqdn string) (string, error)
	IpAliasGet(ipId string, ipAliasId string) (map[string]string, error)
}

type Checker interface {
	Check() error
}

type IpamBuilder interface {
	Build(url string, skipSslValidation bool, username string, password string) Ipam
}
