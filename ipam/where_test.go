package ipam

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNoWhereClause(t *testing.T) {
	assert.Equal(t, "", fmt.Sprintf("%s", Where{}), "Where with no clause should return an empty string")
}

func TestOneConditionEmptyValue(t *testing.T) {
	where := Where{}

	assert.Equal(t, "", fmt.Sprintf("%s", where.Condition("foo", "")), "Where with one condition and no value should return an empty string")
}

func TestOneCondition(t *testing.T) {
	where := &Where{}

	assert.Equal(t,
		"foo='bar'",
		fmt.Sprintf("%s", where.Condition("foo", "bar")),
		"Where with one condition should return the condition")
}

func TestThreeConditionsAndOr(t *testing.T) {
	where := &Where{}

	assert.Equal(t,
		"foo='bar' AND foo2='bar2' OR foo3='bar3'",
		fmt.Sprintf("%s", where.Condition("foo", "bar").And().Condition("foo2", "bar2").Or().Condition("foo3", "bar3")),
		"Where with three condition should return the condition as a string")
}

func TestThreeConditionsAndOrWithOneEmpty(t *testing.T) {
	where := &Where{}

	assert.Equal(t,
		"foo='bar' OR foo3='bar3'",
		fmt.Sprintf("%s", where.Condition("foo", "bar").And().Condition("foo2", "").Or().Condition("foo3", "bar3")),
		"Where with three condition should return the condition as a string")
}

func TestTwoConditionsWithFirstEmpty(t *testing.T) {
	where := &Where{}

	assert.Equal(t,
		"foo2='bar2'",
		fmt.Sprintf("%s", where.Condition("foo", "").And().Condition("foo2", "bar2")),
		"Where with three condition should return the condition as a string")
}

func TestTwoConditionsWithFirstEmptyOr(t *testing.T) {
	where := &Where{}

	assert.Equal(t,
		"foo='bar'",
		fmt.Sprintf("%s", where.Condition("foo2", "").Or().Condition("foo", "bar")))
}
