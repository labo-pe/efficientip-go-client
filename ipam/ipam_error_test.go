package ipam

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestIpAlreadyExistsError(t *testing.T) {
	err := IpAlreadyExists{Name: "test", Ip: "10.10.10.10"}

	assert.Equal(t, "test", err.Name)
	assert.Equal(t, "10.10.10.10", err.Ip)
}
