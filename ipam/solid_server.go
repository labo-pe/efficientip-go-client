package ipam

import (
	"crypto/tls"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	b64 "encoding/base64"
)

type SolidServerBuilder struct{}

func (ssb *SolidServerBuilder) Build(url string, skipSslValidation bool, username string, password string) Ipam {
	return SolidServer{
		Url:               url,
		SkipSslValidation: skipSslValidation,
		Username:          username,
		Password:          password,
	}
}

type SolidServer struct {
	Url               string
	SkipSslValidation bool
	Username          string
	Password          string
}

func (s SolidServer) Check() error {
	_, err := s.IpPoolList("", 1)
	return err
}

func (s SolidServer) ReserveIP(name string, ip string, poolName string, siteName string, class string, classParameters *map[string]string) (map[string]string, error) {
	var ipToAttribute, address string
	var returnEmpty map[string]string

	if name != "" || ip != "" {
		addressObtenue, err := s.getAddress(name, ip)
		if err != nil {
			return returnEmpty, err
		}
		address = addressObtenue
	}

	if address == "" {
		if ip != "" {
			ipToAttribute = ip
		} else {
			pool, err := s.IpPoolList(poolName, 1)
			if err != nil {
				return returnEmpty, err
			} else if len(pool) == 0 {
				return returnEmpty, PoolNotFound{Pool: poolName, Occurences: len(pool)}
			}
			ipFree, err := s.IpFindFree(pool[0]["pool_id"])
			if err != nil {
				return returnEmpty, err
			}
			ipToAttribute = ipFree[0]["hostaddr"]
		}

		if _, err := s.IpAdd(siteName, ipToAttribute, name, class, classParameters); err != nil {
			return returnEmpty, err
		}
	} else {
		ipToAttribute = address
	}

	ipList, err := s.IpAddressList("", ipToAttribute, 1)
	if err != nil {
		return returnEmpty, err
	}

	if len(ipList) == 1 {
		return ipList[0], nil
	} else {
		return returnEmpty, IpNotFound{Ip: ipToAttribute, Occurences: len(ipList)}
	}
}

func (s SolidServer) DeleteIp(ip string) error {
	ipList, err := s.IpAddressList("", ip, 1)
	if err != nil {
		return err
	}

	if len(ipList) == 1 && ipList[0]["type"] != "free" {
		_, err := s.IpDelete(ipList[0]["ip_id"])
		if err != nil {
			return err
		}
	}
	return nil
}

func (s SolidServer) GetIp(ipId string) (map[string]string, error) {
	if ipId != "" {
		var where = Where{}
		var queryString = map[string]string{
			"WHERE": fmt.Sprintf("%s", where.Condition("ip_id", ipId)),
			"limit": "1"}

		response, err := s.request("GET", "rest/ip_address_list", queryString)
		if err != nil {
			return nil, err
		}

		var result []map[string]string
		data, _ := ioutil.ReadAll(response.Body)
		json.Unmarshal([]byte(data), &result)
		response.Body.Close()

		if len(result) == 1 {
			return result[0], nil
		}
	}
	return nil, nil
}

func (s SolidServer) AddAlias(ip string, fqdn string) (string, error) {
	ipList, err := s.IpAddressList("", ip, 1)
	if err != nil {
		return "", err
	}
	if len(ipList) != 1 {
		return "", IpNotFound{Ip: ip, Occurences: len(ipList)}
	}

	aliasId, err := s.getAliasId(ipList[0]["ip_id"], fqdn)
	if err != nil {
		return "", err
	}

	if aliasId == "" {
		rrList, err := s.DnsRRList(fqdn)
		if err != nil {
			return "", err
		} else if len(rrList) > 0 {
			return "", errors.New("DNS RR already exists for this fqdn")
		} else {
			ip := ipList[0]
			_, err := s.IpAliasAdd(fqdn, ip["hostaddr"], ip["site_name"])
			if err != nil {
				return "", err
			} else {
				aliasId, err = s.getAliasId(ip["ip_id"], fqdn)
				if err != nil {
					return "", err
				} else {
					return aliasId, err
				}
			}
		}
	} else {
		return aliasId, nil
	}
}

func (s SolidServer) getAliasId(ipId string, fqdn string) (string, error) {
	aliasList, err := s.IpAliasList(ipId)
	if err != nil {
		return "", err
	}
	for _, alias := range aliasList {
		if alias["alias_name"] == fqdn {
			return alias["ip_name_id"], nil
		}
	}
	return "", nil
}

func (s SolidServer) DeleteAlias(ip string, fqdn string) error {
	ipList, err := s.IpAddressList("", ip, 1)
	if err != nil {
		return err
	}

	if len(ipList) == 1 {
		aliasList, err := s.IpAliasList(ipList[0]["ip_id"])
		if err != nil {
			return err
		}

		for _, alias := range aliasList {
			if alias["alias_name"] == fqdn {
				_, err = s.IpAliasDelete(alias["ip_name_id"])
				return err
			}
		}
	}
	return nil
}

func (s SolidServer) getAddress(name string, ip string) (string, error) {
	var address string

	if ip != "" {
		ipList, err := s.IpAddressList("", ip, 1)

		if err != nil {
			return "", err
		}
		if len(ipList) != 0 && ipList[0]["type"] != "free" {
			if name != ipList[0]["name"] {
				return "", IpAlreadyExists{Name: ipList[0]["name"], Ip: ip}
			} else {
				address = ip
			}
		}
	} else {
		addresses, err := s.IpAddressList(name, ip, 1)
		if err != nil {
			return "", err
		} else {
			if len(addresses) != 0 {
				address = addresses[0]["hostaddr"]
			}
		}
	}
	return address, nil
}

func (s SolidServer) request(method string, ipamCommand string, queryString map[string]string) (*http.Response, error) {
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: s.SkipSslValidation},
	}
	client := &http.Client{Transport: tr}

	request, err := http.NewRequest(method, s.Url+"/"+ipamCommand, nil)
	if err != nil {
		return nil, err
	}

	u := request.URL
	query := u.Query()
	for key, value := range queryString {
		query.Add(key, value)
	}
	u.RawQuery = query.Encode()

	request.Header = map[string][]string{
		"X-IPM-Username": {b64.StdEncoding.EncodeToString([]byte(s.Username))},
		"X-IPM-Password": {b64.StdEncoding.EncodeToString([]byte(s.Password))},
	}

	response, err := client.Do(request)
	if err != nil {
		return response, err
	}
	if (response.StatusCode < 200) || (response.StatusCode >= 300) {
		message, _ := ioutil.ReadAll(response.Body)
		return response, SolidServerError{
			StatusCode: response.StatusCode,
			Method:     method,
			Endpoint:   ipamCommand,
			Parameters: queryString,
			Message:    message}
	}

	return client.Do(request)
}

func (s SolidServer) IpPoolList(poolName string, limit int) ([]map[string]string, error) {
	var where = Where{}
	var queryString = map[string]string{
		"WHERE": fmt.Sprintf("%s", where.Condition("pool_name", poolName)),
		"limit": strconv.Itoa(limit)}

	response, err := s.request("GET", "rest/ip_pool_list", queryString)
	if err != nil {
		return nil, err
	}

	var result []map[string]string
	data, _ := ioutil.ReadAll(response.Body)
	json.Unmarshal([]byte(data), &result)
	response.Body.Close()
	return result, nil
}

func (s SolidServer) IpFindFree(poolId string) ([]map[string]string, error) {
	var queryString = map[string]string{"pool_id": poolId, "max_find": "1"}

	response, err := s.request("GET", "rpc/ip_find_free_address", queryString)
	if err != nil {
		return nil, err
	}

	var result []map[string]string
	data, _ := ioutil.ReadAll(response.Body)
	json.Unmarshal([]byte(data), &result)
	response.Body.Close()

	if len(result) == 0 {
		return result, NoFreeIpFound{PoolId: poolId}
	}

	return result, nil
}

func (s SolidServer) IpAdd(siteName string, ip string, name string, class string, classParameters *map[string]string) ([]map[string]string, error) {
	var queryString = map[string]string{
		"site_name":           siteName,
		"hostaddr":            ip,
		"name":                name,
		"ip_class_name":       class,
		"ip_class_parameters": classParametersToString(classParameters)}

	response, err := s.request("POST", "rest/ip_add", queryString)
	if err != nil {
		return nil, err
	}

	var result []map[string]string
	data, _ := ioutil.ReadAll(response.Body)
	json.Unmarshal([]byte(data), &result)
	response.Body.Close()
	return result, nil
}

func (s SolidServer) IpAddressList(name string, ip string, limit int) ([]map[string]string, error) {
	var where = Where{}
	var queryString = map[string]string{
		"WHERE": fmt.Sprintf("%s", where.Condition("name", name).Or().Condition("hostaddr", ip)),
		"limit": strconv.Itoa(limit)}

	response, err := s.request("GET", "rest/ip_address_list", queryString)
	if err != nil {
		return nil, err
	}

	var result []map[string]string
	data, _ := ioutil.ReadAll(response.Body)
	json.Unmarshal([]byte(data), &result)
	response.Body.Close()

	return result, nil
}

func (s SolidServer) IpDelete(ipId string) ([]map[string]string, error) {
	var queryString = map[string]string{"ip_id": ipId}

	response, err := s.request("DELETE", "rest/ip_delete", queryString)
	if err != nil {
		return nil, err
	}

	var result []map[string]string
	data, _ := ioutil.ReadAll(response.Body)
	json.Unmarshal([]byte(data), &result)
	response.Body.Close()

	return result, nil
}

func (s SolidServer) IpAliasList(ipId string) ([]map[string]string, error) {
	var queryString = map[string]string{"ip_id": ipId}

	response, err := s.request("GET", "rest/ip_alias_list", queryString)
	if err != nil {
		return nil, err
	}

	var result []map[string]string
	data, _ := ioutil.ReadAll(response.Body)
	json.Unmarshal([]byte(data), &result)
	response.Body.Close()

	return result, nil
}

func (s SolidServer) IpAliasGet(ipId string, ipAliasId string) (map[string]string, error) {
	var where = Where{}
	var queryString = map[string]string{
		"ip_id": ipId,
		"WHERE": fmt.Sprintf("%s", where.Condition("ip_name_id", ipAliasId)),
	}

	response, err := s.request("GET", "rest/ip_alias_list", queryString)
	if err != nil {
		return nil, err
	}

	var result []map[string]string
	data, _ := ioutil.ReadAll(response.Body)
	json.Unmarshal([]byte(data), &result)
	response.Body.Close()

	if len(result) != 0 {
		return result[0], nil
	} else {
		return map[string]string{}, nil
	}
}

func (s SolidServer) IpAliasAdd(fqdn string, ip string, siteName string) ([]map[string]string, error) {
	var queryString = map[string]string{
		"ip_name":      fqdn,
		"hostaddr":     ip,
		"site_name":    siteName,
		"ip_name_type": "CNAME"}

	response, err := s.request("POST", "rest/ip_alias_add", queryString)
	if err != nil {
		return nil, err
	}

	var result []map[string]string
	data, _ := ioutil.ReadAll(response.Body)
	json.Unmarshal([]byte(data), &result)
	response.Body.Close()

	return result, nil
}

func (s SolidServer) IpAliasDelete(ipNameId string) ([]map[string]string, error) {
	var queryString = map[string]string{"ip_name_id": ipNameId}

	response, err := s.request("DELETE", "rest/ip_alias_delete", queryString)
	if err != nil {
		return nil, err
	}

	var result []map[string]string
	data, _ := ioutil.ReadAll(response.Body)
	json.Unmarshal([]byte(data), &result)
	response.Body.Close()

	return result, nil
}

func (s SolidServer) DnsRRList(dnsName string) ([]map[string]string, error) {
	var where = Where{}
	var queryString = map[string]string{
		"WHERE": fmt.Sprintf("%s", where.Condition("rr_full_name", dnsName)),
	}

	response, err := s.request("GET", "rest/dns_rr_list", queryString)
	if err != nil {
		return nil, err
	}

	var result []map[string]string
	data, _ := ioutil.ReadAll(response.Body)
	json.Unmarshal([]byte(data), &result)
	response.Body.Close()

	return result, nil
}

func classParametersToString(classParameters *map[string]string) (paramString string) {
	if classParameters != nil && len(*classParameters) > 0 {
		for k, v := range *classParameters {
			paramString += k + "=" + v + "&"
		}
		paramString = paramString[:len(paramString)-1]
	}
	return
}
